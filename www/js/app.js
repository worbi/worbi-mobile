// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('worbi', ['ionic', 'ionic-material', 'ionMdInput']);

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            }
        }
    })
    .state('app.recover', {
        url: '/recover',
        views: {
            'menuContent': {
                templateUrl: 'templates/recover-password.html',
                controller: 'LoginCtrl'
            }
        }
    })

    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })

    .state('app.admin-create-user', {
        url: '/admin/create-user',
        views: {
            'menuContent': {
                templateUrl: 'templates/admin/create_user.html',
                controller: 'AdminCtrl'
            }
        }
    })
    .state('app.admin-search', {
        url: '/admin/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/admin/search_user.html',
                controller: 'SearchUserCtrl'
            }
        }
    })
    .state('app.admin-profile', {
        url: '/admin/profile/:idUser',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/admin/profile.html'
            }
        }
    })

    .state('app.profile', {
        url: '/profile',
        cache: false,
        params: {
            idUser: null
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/user-profile.html',
                controller: 'ProfileCtrl'
            }
        }
    })
    .state('app.profile-edit', {
        url: '/profile/edit',
        views: {
            'menuContent': {
                templateUrl: 'templates/user-profile-edit.html',
                controller: 'ProfileCtrl'
            }
        }
    })
    .state('app.search-coach', {
        url: '/search/coach',
        views: {
            'menuContent': {
                templateUrl: 'templates/search/search-coach.html',
                controller: 'SearchUserCtrl'
            }
        }
    })
    .state('app.search-athlete', {
        url: '/search/athlete',
        views: {
            'menuContent': {
                templateUrl: 'templates/search/search-athlete.html',
                controller: 'SearchUserCtrl'
            }
        }
    })
    .state('app.password', {
        url: '/password',
        views: {
            'menuContent': {
                templateUrl: 'templates/user-password.html',
                controller: 'ProfileCtrl'
            }
        }
    })
    .state('app.add-workout', {
        url: '/workout/add',
        cache: false,
        params: {
            idAthlete: null
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/workout/add-workout.html',
                controller: 'WorkoutCtrl'
            }
        }
    })
    .state('app.edit-workout', {
        url: '/workout/edit',
        cache: false,
        params: {
            idAthlete: null
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/workout/view-workout.html',
                controller: 'WorkoutEditCtrl'
            }
        }
    })
    .state('app.add-workout-steps', {
        url: '/workout/addsteps',
        cache: false,
        params: {
            idAthlete: null,
            idWorkout: null
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/workout/add-workout-steps.html',
                controller: 'WorkoutCtrl'
            }
        }
    })
    ;
    
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});
