'use strict';

app.factory('AppConfig', AppConfig);
AppConfig.$inject = [];

function AppConfig() {
	
	//var serviceHost = 'https://worbi-service.herokuapp.com';
	var serviceHost = 'http://localhost:8080';
	
	if(ionic.Platform.is('BROWSER')) {
		serviceHost = 'http://localhost:8080';
	}


	var sports = [
        { id : 1, name: 'Bike' , label: 'Ciclismo' },
        { id : 2, name: 'Run'  , label: 'Corrida' },
        { id : 3, name: 'Swim' , label: 'Natação' },
        { id : 4, name: 'Gym'  , label: 'Musculação' }
    ];

    var weekdays = [
        { id : 1, name: 'dom' , label: 'Domingo' },
        { id : 2, name: 'seg' , label: 'Segunda' },
        { id : 3, name: 'ter' , label: 'Terça' },
        { id : 4, name: 'qua' , label: 'Quarta' },
        { id : 5, name: 'qui' , label: 'Quinta' },
        { id : 6, name: 'sex' , label: 'Sexta' },
        { id : 7, name: 'sab' , label: 'Sábado' }
    ];


	var appConf = {

		loggedUserKey : 'loggedUser',
		serviceLayer : serviceHost + '/services/v1/',
		services : {
			post : {
				login : 'login'
			}
		},

		sports : sports,
		weekdays : weekdays
		
	};

	return appConf;
}