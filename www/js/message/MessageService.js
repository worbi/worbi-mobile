'use strict'

app.factory('MessageService', MessageService);

MessageService.$inject = ['AppConfig', '$http'];

function MessageService(AppConfig, $http) {

    var urlBase = AppConfig.serviceLayer;
    var messageService = {};


    var services = {
        post : {
            save : urlBase + 'message'
        },
        get  : {
            fromUser       : urlBase + 'message/from/{idFromUser}',
            toUser         : urlBase + 'message/to/{idToUser}',
            fromUserToUser : urlBase + 'message/from/{idFromUser}/to/{idToUser}'
        }, 
        delete :{
            
        }
    }

    messageService.save = (message) => {
        return $http.post(services.post.save, message);
    }

    messageService.listFrom = (idFromUser) => {
        var uri = services.get.fromUser.replace("{idFromUser}",idFromUser); 
        return $http.get(uri);
    }

    messageService.listTo = (idToUser) => {
        var uri = services.get.toUser.replace("{idToUser}",idToUser); 
        return $http.get(uri);
    }

    messageService.listFromTo = (idFromUser, idToUser) => {
        var uri = services.get.fromUserToUser.replace("{idFromUser}",idFromUser).replace("{idToUser}",idToUser); 
        return $http.get(uri);
    }

    return messageService;
}