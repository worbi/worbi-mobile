'use strict';

app.controller('MessageCtrl', MessageCtrl);
MessageCtrl.$inject = ['$scope', '$q', 'Formatter', 'LoadingHelper', 'AlertHelper', 'MessageService'];

function MessageCtrl($scope, $q, Formatter, LoadingHelper, AlertHelper, MessageService) {

	this.sendMessage = (message) => {
		message.sentDate = moment().format();
		LoadingHelper.show();
		$q.all({
            message : MessageService.save(message)
        }).then( d => {
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Erro ao enviar mensagem!");
        });
	}

	this.replyMessage = (message) => {

		if (message.reply != null) {
			var replyMessage = {
				to   : message.from,
				from : message.to,
				message : message.reply + '<br/>---<br/>' + message.message,
				sentDate : moment().format()
			}
			LoadingHelper.show();
			$q.all({
	            message : MessageService.save(replyMessage)
	        }).then( d => {
	            LoadingHelper.hide();
	        }, err => {
	            console.error(err);
	            LoadingHelper.hide();
	            AlertHelper.error("Erro ao responder mensagem!");
	        });
		}

	}
}
