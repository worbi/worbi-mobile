'use strict';

app.controller('SearchUserCtrl', SearchUserCtrl);
SearchUserCtrl.$inject = ['$scope', '$q', '$state', '$stateParams', 'AlertHelper', 'LoadingHelper', 'ProfileService', 'LoginService'];

function SearchUserCtrl($scope, $q, $state, $stateParams, AlertHelper, LoadingHelper, ProfileService, LoginService) {

	this.userSearch = null;
	this.loggedId;

	var _this = this;
	LoginService.getLoggedUser((user)=> {
		_this.loggedId = user.idUser;
	});

	this.searchUser = (query) => {
		var errorMessage = 'Erro ao buscar usuário!';
		var t = this;

		LoadingHelper.show();

		$q.all({
			profileData : ProfileService.list(query)
        }).then( d => {
			LoadingHelper.hide();
			console.log(d.profileData.data);
			t.userSearch = d.profileData.data;
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error(errorMessage);
        });
	}

	this.searchCoach = (query) => {
		var errorMessage = 'Erro ao buscar Treinador!';
		var t = this;

		LoadingHelper.show();

		$q.all({
			profileData : ProfileService.listCoaches(query)
        }).then( d => {
			LoadingHelper.hide();
			console.log(d.profileData.data);
			t.userSearch = d.profileData.data;
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error(errorMessage);
        });
	}

	this.searchAthlete = (query) => {
		var errorMessage = 'Erro ao buscar Atleta!';
		var t = this;

		LoadingHelper.show();

		$q.all({
			profileData : ProfileService.listAthletes(query)
        }).then( d => {
			LoadingHelper.hide();
			console.log(d.profileData.data);
			t.userSearch = d.profileData.data;
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error(errorMessage);
        });
	}


}
