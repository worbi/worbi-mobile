'use strict';

app.controller('AdminCtrl', AdminCtrl);

AdminCtrl.$inject = ['$scope', '$q', '$state', 'AppConfig', 'AlertHelper', 'LoadingHelper', 'UserService', 'AthleteService', 'CoachService', 'Formatter'];
function AdminCtrl($scope, $q, $state, AppConfig, AlertHelper, LoadingHelper, UserService, AthleteService, CoachService, Formatter) {    

    this.clear = () => {
        this.user = { 
            profile : {
                admin : false,
                type  : ''
            }
        };
    }
    this.clear();

    var ctrl = this;
    $scope.$watch("ctrl.user.profile.admin", (newValue, oldValue) => {
        this.user.profile.type = '';
        this.user.athlete = {};
        this.user.coach   = {};
    });

    this.sports = AppConfig.sports;
    this.weekdays = AppConfig.weekdays;

    this.getCoach = (vo) => {

        if (vo.profile.type == 'coach') {
            var coach = {
                cref : vo.coach.cref,
                credential : {
                    idUser : vo.idUser
                },
                sports : []
            };
            angular.forEach(vo.coach.sports, function(value, key) {
                if (value) {
                    coach.sports.push({ idSport: key });
                }
            });

            return coach;

        } else {
            return null;
        }

    };

    this.getAthlete = (vo) => {

        if (vo.profile.type == 'athlete') {

            var dateSend = Formatter.formatDate(vo.athlete.birthdate); 
            var heightSend = Formatter.formatNumber(vo.athlete.height);
            var weightSend = Formatter.formatNumber(vo.athlete.weight);

            var athlete = {
                birthDate : dateSend,
                gender    : vo.athlete.gender,
                height    : heightSend,
                weight    : weightSend,
                credential : {
                    idUser : vo.idUser
                },
                sports : [],
                trainingWeekdays : []
            }

            angular.forEach(vo.athlete.sports, function(value, key) {
                if (value) {
                    athlete.sports.push({ idSport: key });
                }
            });

            angular.forEach(vo.athlete.weekday, function(value, key) {
                if (value) {
                    athlete.trainingWeekdays.push({ weekday: key });
                }
            });
            
            return athlete;

        } else {
            return null;
        }
    };

    this.salvar = (vo) => {
        console.log(vo);

        var errorMessage = 'Erro ao salvar usuário';
        var successMessage = 'Usuário cadastrado com sucesso!';

        var currentUser;

        var user = {
            name     : vo.nome,
            email    : vo.email,
            password : vo.password,
            isAdmin  : vo.profile.admin
        };

        var athlete = this.getAthlete(vo);
        var coach   = this.getCoach(vo);

        if (user.email != null) {

            LoadingHelper.show();

            $q.all({ 
                userData : UserService.save(user) 
            }).then( d => {
                console.log(d);
                var user = d.userData.data;

                console.log(athlete);
                console.log(coach);
                console.log(user);

                if (user.isAdmin) {
                    LoadingHelper.hide();
                    AlertHelper.info(successMessage);
                    $state.go('app.home');
                } else {

                    if (athlete != null) {
                        athlete.credential = { idUser : user.idUser };
                    }
                    if (coach != null) {
                        coach.credential = { idUser : user.idUser };
                    }

                    $q.all({
                        athleteData : AthleteService.save(athlete),
                        coachData : CoachService.save(coach)
                    }).then( d => {
                        LoadingHelper.hide();
                        console.log(d);

                        if (d.athleteData != null) {
                            successMessage = 'Atleta cadastrado com sucesso!';
                        } else if (d.coachData != null) {
                            successMessage = 'Treinador cadastrado com sucesso!';
                        }

                        AlertHelper.info(successMessage);
                        $state.go('app.home');
                    }, err => {
                        console.error(err);
                        LoadingHelper.hide();
                        AlertHelper.error(errorMessage);

                        console.log('delete');
                        console.log(user);                        
                        UserService.delete(user);

                    });
                }

            }, err => {
                console.error(err);
                LoadingHelper.hide();
                if (err.status == 409) {
                    errorMessage += "<br>Email já cadastrado";
                    return;
                }
                AlertHelper.error(errorMessage);
            });
        }

    }

}