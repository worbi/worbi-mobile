'use strict';

app.controller('HomeCtrl', HomeCtrl);
HomeCtrl.$inject = ['$scope','$q', 'LoadingHelper', 'ActionsHelper', 'NavigationHelper', 'LoginService', 'ProfileService', 'WorkoutService'];

function HomeCtrl($scope, $q, LoadingHelper, ActionsHelper, NavigationHelper, LoginService, ProfileService, WorkoutService) {   

	this.profile = {};
	this.workouts = {};	

	LoginService.isAdmin((admin) =>{
        $scope.isAdmin = admin;
    });

	this.loadWorkoutTab = () => {

		LoginService.getLoggedUser((user)=> {

			var _this = this;
			LoadingHelper.show();
			$q.all({
				profileData : ProfileService.get(user.idUser)
	        }).then( d => {
				LoadingHelper.hide();
				_this.profile = d.profileData.data;

				if (_this.profile.isAthlete) {
					LoadingHelper.show();

					$q.all({
						workouts : WorkoutService.listByAthlete(_this.profile.athlete.idAthlete)
			        }).then( d => {
						_this.workouts = d.workouts.data;

						angular.forEach(_this.workouts, function(value, key) {
							value.scheduleDate = moment(value.scheduleDate).format('DD/MM/YYYY');
			            });

						LoadingHelper.hide();
						$scope.$broadcast('scroll.refreshComplete');
					});
				} else {
					$scope.$broadcast('scroll.refreshComplete');
				}

			});
		});
	}

	this.reloadMessage = () => {
		console.log("reload messages");
	}

	this.showOptionsCoach = () => {
		var buttons = [
            { 
                text: '<i class="icon fas fa-search fa-fw"></i> Procurar Atletas', 
                action: () => {
                    NavigationHelper.goToState('app.search-athlete', null, null)
                }
            }
        ];

        ActionsHelper.show(buttons);
	}

	this.loadWorkoutTab();
}
