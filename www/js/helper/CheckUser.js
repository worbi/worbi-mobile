'use strict';

app.directive('checkUser', CheckUser);

function CheckUser() {
	return {
		template: '',
		restrict: 'E',
        scope : {
            ngModel : '='
        },
		controller: CheckUserCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

CheckUserCtrl.$inject = ['$scope', '$state', 'LoginService', '$timeout', '$ionicHistory', 'LoadingHelper'];

function CheckUserCtrl($scope, $state, LoginService, $timeout, $ionicHistory, LoadingHelper) {

    LoginService.isLogged(function(logged) {
        if (!logged) {
            $state.go('app.login');
        }
    });


}
