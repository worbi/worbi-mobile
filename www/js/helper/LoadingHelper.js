'use strict';

app.service('LoadingHelper', LoadingHelper);

LoadingHelper.$inject = ['$ionicLoading'];

function LoadingHelper($ionicLoading) {


	this.show = () => {
		$ionicLoading.show({
        	template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>'
        });
	}

	this.hide = () => {
		$ionicLoading.hide();
	}


}

