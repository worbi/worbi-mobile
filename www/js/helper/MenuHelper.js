'use strict';

app.service('MenuHelper', MenuHelper);

MenuHelper.$inject = ['AppConfig'];

function MenuHelper(AppConfig) {


	this.getMenu = (admin) => {

        var menuLinks = [];

		menuLinks = [];
    	if (admin) {
    		menuLinks.push({link : '#/app/admin'  , label: 'Painel Administrador'});
    		menuLinks.push({link : '#/app/profile', label: 'Perfil'});
    	} else {
			menuLinks.push({link : '#/app/workouts' , label: 'Treinamentos'});
			menuLinks.push({link : '#/app/profile'  , label: 'Perfil'});
    	} 

    	return menuLinks;		
	}

	this.getName = (user) => {

		if (user.name != null) {
			return user.name;
		} else {
			return user.email;
		}
	}

}