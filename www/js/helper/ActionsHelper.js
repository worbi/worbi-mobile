'use strict';

app.service('ActionsHelper', ActionsHelper);

ActionsHelper.$inject = ['$ionicActionSheet'];

function ActionsHelper($ionicActionSheet) {

	this.show = (_buttons) => {
		var options = $ionicActionSheet.show({
			titleText: 'Ações',

			buttons: _buttons,

			buttonClicked: function(index) {
				if(_buttons[index].action) {
		            _buttons[index].action();
		        }
				return true;
			},
			destructiveButtonClicked: function() {
				console.log('DESTRUCT');
				return true;
			}
		});

		return options;
	}

}
