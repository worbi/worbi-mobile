'use strict';

app.service('AlertHelper', AlertHelper);

AlertHelper.$inject = ['AppConfig', '$ionicPopup'];

function AlertHelper(AppConfig, $ionicPopup) {


	this.alert = (_title, _message) => {
		var alertPopup = $ionicPopup.alert({
            title: _title,
            template: _message
        });
		return alertPopup;
	}

	this.info = (_message) => {

		var templ = "<table>"
        				+"<tbody>"
    					+"<tr>"
            				+"<td style=\"vertical-align: middle;padding-right: 15px;font-size: 30px;\">"
    							+"<i class=\"fas fa-info-circle\" aria-hidden=\"true\"></i>"
            				+"</td>"
            				+"<td>"
                				+"<span>" + _message + "</span>"
        					+"</td>"
    					+"</tr>"
					+"</tbody></table>";

		var alertPopup = $ionicPopup.alert({
            title: "Informação",
            template: templ,
            buttons: [
		      {
		        text: '<b>Ok</b>',
		        type: 'btn-primary'
		      }
    		]
        });

		return alertPopup;
	}

	this.error = (_message) => {

		var templ = "<table>"
        				+"<tbody>"
    					+"<tr>"
            				+"<td style=\"vertical-align: middle;padding-right: 15px;font-size: 30px;\">"
    							+"<i class=\"fas fa-exclamation-circle\" aria-hidden=\"true\"></i>"
            				+"</td>"
            				+"<td>"
                				+"<span>" + _message + "</span>"
        					+"</td>"
    					+"</tr>"
					+"</tbody></table>";

		var alertPopup = $ionicPopup.alert({
            title: "OPS!",
            template: templ,
            buttons: [
		      {
		        text: '<b>Ok</b>',
		        type: 'btn-danger'
		      }
    		]
        });

		return alertPopup;
	}

	this.show = () => {
		$ionicLoading.show({
        	template: AppConfig.loadingTemplate
        });
	}

	this.hide = () => {
		$ionicLoading.hide();
	}


}
