'use strict';

app.service('DialogHelper', DialogHelper);

DialogHelper.$inject = ['AppConfig', '$ionicPopup'];

function DialogHelper(AppConfig, $ionicPopup) {


	this.alert = (_title, _message) => {
		var alertPopup = $ionicPopup.alert({
            title: _title,
            template: _message
        });
		return alertPopup;
	}

	this.confirm = (_message) => {

		var templ = "<table>"
        				+"<tbody>"
    					+"<tr>"
            				+"<td style=\"vertical-align: middle;padding-right: 15px;font-size: 30px;\">"
    							+"<i class=\"fas fa-question-circle-o\" aria-hidden=\"true\"></i>"
            				+"</td>"
            				+"<td>"
                				+"<span>" + _message + "</span>"
        					+"</td>"
    					+"</tr>"
					+"</tbody></table>";

		var alertPopup = $ionicPopup.prompt({
            title: "Confirmar",
            template: templ,
            buttons: [
              {
                text: '<b>Não</b>',
                onTap: (e) => { return false; }
              },
		      {
		        text: '<b>Sim</b>',
		        type: 'btn-primary',
                onTap: (e) => { return true; }
		      }
    		]
        });

		return alertPopup;
	}

    this.remove = (_message) => {

        var templ = "<table>"
                        +"<tbody>"
                        +"<tr>"
                            +"<td style=\"vertical-align: middle;padding-right: 15px;font-size: 30px;\">"
                                +"<i class=\"fas fa-question-circle-o\" aria-hidden=\"true\"></i>"
                            +"</td>"
                            +"<td>"
                                +"<span>" + _message + "</span>"
                            +"</td>"
                        +"</tr>"
                    +"</tbody></table>";

        var alertPopup = $ionicPopup.prompt({
            title: "Excluir",
            template: templ,
            buttons: [
              {
                text: '<b>Não</b>',
                onTap: (e) => { return false; }
              },
              {
                text: '<b>Sim</b>',
                type: 'btn-danger',
                onTap: (e) => { return true; }
              }
            ]
        });

        return alertPopup;
    }

}
