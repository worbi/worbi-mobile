'use strict';

app.service('ModalHelper', ModalHelper);

ModalHelper.$inject = ['$ionicModal'];

function ModalHelper($ionicModal) {

	this.show = (template, scope) => {
		var modal = $ionicModal.fromTemplateUrl(template,
                    {
                        scope: scope
                    });

        modal.then((modal)=>{
        	scope.modal = modal;
            modal.show();
        });
	}

}