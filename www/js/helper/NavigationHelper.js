'use strict';

app.service('NavigationHelper', NavigationHelper);

NavigationHelper.$inject = ['$state', '$timeout', '$ionicHistory', '$window', 'LoadingHelper', 'AlertHelper'];

function NavigationHelper($state, $timeout, $ionicHistory, $window, LoadingHelper, AlertHelper) {

	this.goToState = (state, params, message) => {
		LoadingHelper.show();
        $timeout(function () {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({ disableBack: false, historyRoot: false, reload: true, inherit: false });

            $state.transitionTo(state, params, {reload: true, notify:true});
            LoadingHelper.hide();

            if (message != null) {
            	AlertHelper.info(success);
            }
        }, 300);
	}

	this.goToStateClear = (state, params, message) => {
		LoadingHelper.show();
        $timeout(function () {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: false, reload: true, inherit: false });
            
			$state.transitionTo(state, params, {reload: true, notify:true});
            LoadingHelper.hide();

            if (message != null) {
            	AlertHelper.info(message);
            }

        }, 300);
	}

}