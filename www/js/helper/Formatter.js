'use strict';

app.service('Formatter', Formatter);

Formatter.$inject = [];

function Formatter() {

	this.formatDate = (date) => {
		var result = "";
		if (date != null) {
			result = moment(date, "DD/MM/YYYY").format("YYYY-MM-DD") + "T03:00:00.000Z"; 
		}
		return result;
	}

	this.formatNumber = (number) => {
		var result = "";
		if (number != null) {
			result = number.replace(",", ".");
		}
		return result;
	}


}

