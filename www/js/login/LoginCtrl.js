'use strict';

app.controller('LoginCtrl', LoginCtrl);
LoginCtrl.$inject = ['$scope', '$ionicSideMenuDelegate', '$state', '$ionicHistory', '$timeout', '$q', 'LoadingHelper', 'AlertHelper', 'LoginService', 'MenuHelper', 'NavigationHelper', 'UserService'];

function LoginCtrl($scope, $ionicSideMenuDelegate, $state, $ionicHistory, $timeout, $q, LoadingHelper, AlertHelper, LoginService, MenuHelper, NavigationHelper, UserService) {  

	var HOST = window.location.host;

	this.user = {
		email : '',
		password : ''

	};

	$scope.$on('$ionicView.beforeEnter', function (event) {
		$ionicSideMenuDelegate.canDragContent(false);
	});

	$scope.$on('$ionicView.beforeLeave', function (event) {
		$ionicSideMenuDelegate.canDragContent(true);
	});

	$scope.login = function(user) {
		var t = this;
		$scope.clearErrors();

		LoginService.login(user, result => {
			$scope.user = {};

			if(result.status == 200) {
						    
				$timeout(function () {
			        $ionicHistory.clearCache();
			        $ionicHistory.clearHistory();
			        $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: false });
			        $state.go('app.home');

					$scope.$emit('menu-name', {data: MenuHelper.getName(result.data)});
			        $scope.$emit('menu-load', {data: MenuHelper.getMenu(result.data.isAdmin)});

		        }, 300);

			} else if (result.status == 0) {
				$scope.setError("Sistema Indisponível");
			} else if (result.status == 401 || result.status == 404 ) {
				$scope.setError("Login e/ou Senha inválidos.");
			}

		}, error => {
			console.log(error);
		});
	}

	$scope.recoverPassword = (email) => {
		LoadingHelper.show();
		$q.all({
            user : UserService.reset(email)                
        }).then( d => {
            LoadingHelper.hide();
            AlertHelper.info('Senha redefinida com sucesso!');
            NavigationHelper.goToStateClear('app.login', null, null);
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Erro ao redefinir a senha!");
        });   
	}

	$scope.goToRecoverPassword = () => {
		NavigationHelper.goToStateClear('app.recover', null, null);
	}

	$scope.backToLogin = () => {
		NavigationHelper.goToStateClear('app.login', null, null);
	}

	$scope.setError = (message) => {
		$scope.error = true;
		$scope.errorMessage = message;
	}

	$scope.clearErrors = () => {
		$scope.error = false;
		$scope.errorMessage = "";
	}

	$scope.logout = () => {
		console.log('logoute');
		LoginService.logout();
	}


}