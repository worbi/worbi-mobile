
'use strict';

app.service('LoginService', LoginService);

LoginService.$inject = ['AppConfig', '$http', '$state', 'LoadingHelper', '$timeout', '$ionicHistory'];

function LoginService(AppConfig, $http, $state, LoadingHelper, $timeout, $ionicHistory) {
	
	this.login = (user, callback) => {
		LoadingHelper.show();

		var t = this;
		var loginUri = AppConfig.serviceLayer + AppConfig.services.post.login;

		$http.post(loginUri, user).then(result => {
			LoadingHelper.hide();

			localStorage.setItem(AppConfig.loggedUserKey, JSON.stringify(result.data));
			return callback(result);
		}, error => {
			t.logout();
			LoadingHelper.hide();
			return callback(error);
		});
	}

	this.isLogged = (callback) => {
		var isLogged = localStorage.getItem(AppConfig.loggedUserKey);

		if (isLogged != null) {
			return callback(true);
		}

		return callback(false);
	}

	this.getLoggedUser = (callback)	 => {
		this.isLogged((isLogged) => {
			if (isLogged) {
				var userLogged = JSON.parse(localStorage.getItem(AppConfig.loggedUserKey));
				return callback(userLogged);
			}

			return callback(false);
		});
	}

	this.isAdmin = (callback) => {

		this.isLogged((isLogged) => {
			if (isLogged) {
				var userLogged = JSON.parse(localStorage.getItem(AppConfig.loggedUserKey));
				if (userLogged.isAdmin) {
					return callback(true);
				}
			}

			return callback(false);
		});
	}

	this.logout = (callback) => {
		LoadingHelper.show();

		localStorage.removeItem(AppConfig.loggedUserKey);

		$timeout(function () {
	        $ionicHistory.clearCache();
	        $ionicHistory.clearHistory();
	        LoadingHelper.hide();
	        $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: false });
	        $state.go('app.login');
        }, 300);
	}
}