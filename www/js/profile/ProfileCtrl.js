'use strict';

app.controller('ProfileCtrl', ProfileCtrl);
ProfileCtrl.$inject = ['$scope', '$q', '$state', '$stateParams', 'AppConfig','NavigationHelper', 'ActionsHelper', 'AlertHelper', 'LoadingHelper', 'ProfileService', 'LoginService', 'UserService', 'AthleteService', 'CoachService', 'Formatter'];

function ProfileCtrl($scope, $q, $state, $stateParams, AppConfig, NavigationHelper, ActionsHelper, AlertHelper, LoadingHelper, ProfileService, LoginService, UserService, AthleteService, CoachService, Formatter) {

	this.profile = {};
	this.enableEdit = false;

	this.sports = AppConfig.sports;
	this.weekdays = AppConfig.weekdays;

	this.getProfile = (idUser) => {
		var errorMessage = 'Perfil não encontrado!';
		var _this = this;
		$q.all({
			profileData : ProfileService.get(idUser)
        }).then( d => {
			LoadingHelper.hide();

			var profileData = d.profileData.data;
			_this.profile = profileData;

			if (_this.profile.isAthlete) {
				if (_this.profile.athlete.gender == 'm') {
					_this.profile.athlete.genderDesc = 'Masculino';
				} else if (_this.profile.athlete.gender == 'f') {
					_this.profile.athlete.genderDesc = 'Feminino';
				}

				_this.profile.athlete.sportsSelected = [];
				_this.profile.athlete.trainingWeekdaysSelected = [];

				angular.forEach(_this.profile.athlete.sports, function(value, key) {
					_this.profile.athlete.sportsSelected[value.idSport] = true;
	            });
	            angular.forEach(_this.profile.athlete.trainingWeekdays, function(value, key) {
					_this.profile.athlete.trainingWeekdaysSelected[value.weekday] = true;
	            });

	            if (_this.profile.athlete.birthDate != null) {
	            	_this.profile.athlete.birthDate = moment(_this.profile.athlete.birthDate).format('DD/MM/YYYY');
	            }
	            if (_this.profile.athlete.weight != null) {
	            	_this.profile.athlete.weight = String(_this.profile.athlete.weight).replace(".",",");;
	            }
	            if (_this.profile.athlete.height != null) {
	            	_this.profile.athlete.height = String(_this.profile.athlete.height).replace(".",",");;
	            }
			}

			if (_this.profile.isCoach) {
				_this.profile.coach.sportsSelected = [];
				angular.forEach(_this.profile.coach.sports, function(value, key) {
					_this.profile.coach.sportsSelected[value.idSport] = true;
	            });
			}

			LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error(errorMessage);
        });
	}

	if ($stateParams.idUser != null) {

		LoadingHelper.show();
		var idUser = $stateParams.idUser;
		this.getProfile(idUser);

	} else {

		var _this = this;
		LoginService.getLoggedUser((user)=> {
			_this.getProfile(user.idUser);
			_this.enableEdit = true;
		});
	}

	this.getCoach = (vo) => {

        if (vo.isCoach) {
            var coach = {
            	idCoach : vo.coach.idCoach,
                cref : vo.coach.cref,
                credential : vo.user,
                sports : []
            };
            angular.forEach(vo.coach.sportsSelected, function(value, key) {
                if (value) {
                    coach.sports.push({ idSport: key });
                }
            });

            return coach;

        } else {
            return null;
        }

    };

	this.getAthlete = (vo) => {

        if (vo.isAthlete) {

            var dateSend = Formatter.formatDate(vo.athlete.birthDate); 
            var heightSend = Formatter.formatNumber(vo.athlete.height);
            var weightSend = Formatter.formatNumber(vo.athlete.weight);

            var athlete = {
            	idAthlete : vo.athlete.idAthlete,
                birthDate : dateSend,
                gender    : vo.athlete.gender,
                height    : heightSend,
                weight    : weightSend,
                credential : vo.user,
                sports : [],
                trainingWeekdays : []
            }

            angular.forEach(vo.athlete.sportsSelected, function(value, key) {
                if (value) {
                    athlete.sports.push({ idSport: key });
                }
            });

            angular.forEach(vo.athlete.trainingWeekdaysSelected, function(value, key) {
                if (value) {
                    athlete.trainingWeekdays.push({ weekday: key });
                }
            });

            return athlete;

        } else {
            return null;
        }
    };

	this.saveProfile = (profile) => {

        var user = profile.user;
        var athlete = this.getAthlete(profile);
        var coach   = this.getCoach(profile);
		
		LoadingHelper.show();
		$q.all({
			user    : UserService.save(user),
			athlete : AthleteService.save(athlete),
			coach   : CoachService.save(coach)
        }).then( d => {
			LoadingHelper.hide();
			AlertHelper.info("Perfil atualizado");
            NavigationHelper.goToStateClear('app.profile');
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao atualizar perfil");
        });

	}


	this.changePassword = (pass) => {

		if (pass != null) {
			if (pass.newPassword != null && (pass.newPassword.password != pass.newPassword.confirm)) {
				AlertHelper.error("Nova senha não confere!");
			} else {
				if (pass.currentPassword == null || pass.currentPassword.password == "") {
					AlertHelper.error("Senha atual não informada!");
				} else {
					pass.idUser = this.profile.user.idUser;
					LoadingHelper.show();

					$q.all({
						user : UserService.changePassword(pass)
			        }).then( d => {
						LoadingHelper.hide();
						AlertHelper.info("Senha alterada com sucesso!");
                        NavigationHelper.goToStateClear('app.profile');
			        }, err => {
			            console.error(err);
			            LoadingHelper.hide();
			            if (err.status == 401) {
			            	AlertHelper.error("Senha atual não confere!");
			            } else {
			            	AlertHelper.error("Ocorreu um erro ao tentar trocar a senha.");
			            }
			        });

				};
			}
		} else {
			AlertHelper.error("Nenhuma senha informada.");
		}
	}


	this.showOptions = () => {

		var buttons = [
				{ 
					text: '<i class="icon ion-edit"></i> Editar Perfil', 
					action: () => { 
						NavigationHelper.goToState('app.profile-edit');
					}
				},
				{ 
					text: '<i class="icon fas fa-key fa-fw"></i> Trocar Senha', 
					action: () => { 
						NavigationHelper.goToState('app.password');
					}
				}
			];

		ActionsHelper.show(buttons);
	}
}