'use strict'

app.factory('WorkoutService', WorkoutService);

WorkoutService.$inject = ['AppConfig', '$http'];

function WorkoutService(AppConfig, $http) {

    var urlBase = AppConfig.serviceLayer;
    var workoutService = {};


    var services = {
        post : {
            save    : urlBase + 'workout/save',
            addstep : urlBase + 'workout/{idWorkout}/addStep',
            flagWorkout : urlBase + 'workout/{idWorkout}/flag'
        },
        get  : {
            listByAthlete : urlBase + 'workout/athlete/{idAthlete}',
            getOne : urlBase + 'workout/{idWorkout}'
        }, 
        delete :{
            deleteWorkout : urlBase + 'workout/{idWorkout}/del',
            deleteStep : urlBase + 'step/{idStep}/del'
        }
    }

    workoutService.save = (workout) => {
        return $http.post(services.post.save, workout);
    }

    workoutService.addstep = (idWorkout, step) => {
        var uri = services.post.addstep.replace("{idWorkout}",idWorkout); 
        return $http.post(uri, step);
    }

    workoutService.deleteStep = (idStep) => {
        var uri = services.delete.deleteStep.replace("{idStep}",idStep); 
        return $http.post(uri);
    }

    workoutService.flagWorkout = (idWorkout) => {
        var uri = services.post.flagWorkout.replace("{idWorkout}",idWorkout); 
        return $http.post(uri);
    }

    workoutService.deleteWorkout = (idWorkout) => {
        var uri = services.delete.deleteWorkout.replace("{idWorkout}",idWorkout); 
        return $http.post(uri);
    }

    workoutService.getOne = (idWorkout) => {
        var uri = services.get.getOne.replace("{idWorkout}",idWorkout); 
        return $http.get(uri);
    }

    workoutService.listByAthlete = (idAthlete) => {
        var uri = services.get.listByAthlete.replace("{idAthlete}",idAthlete);        
        return $http.get(uri);
    }

    return workoutService;
}