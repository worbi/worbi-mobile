'use strict';

app.controller('WorkoutCtrl', WorkoutCtrl);
WorkoutCtrl.$inject = ['$scope', '$q', '$stateParams', 'ModalHelper', 'LoadingHelper', 'AlertHelper', 'ActionsHelper', 'DialogHelper', 'NavigationHelper', 'Formatter', 'LoginService', 'ProfileService', 'AthleteService', 'WorkoutService'];

function WorkoutCtrl($scope, $q, $stateParams, ModalHelper, LoadingHelper, AlertHelper, ActionsHelper, DialogHelper, NavigationHelper, Formatter, LoginService, ProfileService, AthleteService, WorkoutService) {   

    this.athlete = {};
    this.profile = {};
    this.workout = {};
    this.athlete.sportsSelected = {};
    this.workout = {};

    var _this = this;
    LoginService.getLoggedUser((user)=> {
        LoadingHelper.show();
        $q.all({
            profile : ProfileService.get(user.idUser)
        }).then( d => {
            LoadingHelper.hide();
            _this.profile = d.profile.data;
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao recuperar o Professor!");
        });   
    });

    this.getWorkout = (idWorkout) => {
        var _this = this;
        LoadingHelper.show();
        $q.all({
            workout : WorkoutService.getOne(idWorkout)
        }).then( d => {
            _this.workout = d.workout.data;
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao recuperar o Treino!");
        });
    }

    this.getAthlete = (idAthlete) => {
        var _this = this;
        LoadingHelper.show();
        $q.all({
            athlete : AthleteService.getOne(idAthlete)
        }).then( d => {
            _this.athlete = d.athlete.data;
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao recuperar o Atleta!");
        });
    }

    if ($stateParams.idAthlete != null) {
        var idAthlete = $stateParams.idAthlete;
        this.getAthlete(idAthlete);

        if ($stateParams.idWorkout != null) {
            var idWorkout = $stateParams.idWorkout;
            this.getWorkout(idWorkout);
        }

    } else {
        this.getAthlete(1);
        this.getWorkout(1);
    }

    this.removeStep = (idStep) => {

        $q.all({
            data : DialogHelper.remove("Deseja remover este passo?")
        }).then( d => {
            if (d.data) {
                this.deleteStep(idStep);
            }
        });
    }

    this.goToAddSteps = (idAthlete, workout) => {

        workout.athlete = { idAthlete : idAthlete };
        workout.coach = { idCoach :this.profile.coach.idCoach };
        workout.scheduleDate = Formatter.formatDate(workout.scheduleDate);

        LoadingHelper.show();
        $q.all({
            workout : WorkoutService.save(workout)
        }).then( d => {
            NavigationHelper.goToState('app.add-workout-steps', { idAthlete : idAthlete, idWorkout : d.workout.data.idWorkout });
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao salvar!");
        });
    }

    this.addStepPopup = () => {
        $scope.step = {stepType:1};
        ModalHelper.show('templates/workout/add-step-popup.html', $scope);
    }

    this.deleteStep = (idStep) => {
        LoadingHelper.show();
        $q.all({
            step : WorkoutService.deleteStep(idStep)
        }).then( d => {
            this.getWorkout(this.workout.idWorkout);
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao remover Exercício!");
        });
    }

    this.saveStep = () => {

        LoadingHelper.show();
        $q.all({
            workout : WorkoutService.addstep(this.workout.idWorkout, $scope.step)
        }).then( d => {
            $scope.modal.hide();
            this.getWorkout(d.workout.data.idWorkout);
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao salvar!");
        });
    }

    this.saveWorkout = () => {
        NavigationHelper.goToStateClear('app.home', null, null);
        AlertHelper.info("Treino salvo com sucesso!");
    }

    this.showOptions = () => {

        var buttons = [
            { 
                text: '<i class="icon fas fa-plus-circle fa-fw"></i> Adicionar Exercício', 
                action: () => {
                    this.addStepPopup();
                }
            },
            /*
            { 
                text: '<i class="icon fas fa-repeat fa-fw"></i> Adicionar Repetição', 
                action: () => { 
                    AlertHelper.error("Não Implementado!");
                }
            }*/
        ];

        ActionsHelper.show(buttons);
    }
}