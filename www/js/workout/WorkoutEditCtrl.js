'use strict';

app.controller('WorkoutEditCtrl', WorkoutEditCtrl);
WorkoutEditCtrl.$inject = ['$scope', '$q', '$stateParams', 'ModalHelper', 'LoadingHelper', 'AlertHelper', 'ActionsHelper', 'DialogHelper', 'NavigationHelper', 'Formatter', 'LoginService', 'ProfileService', 'AthleteService', 'WorkoutService'];

function WorkoutEditCtrl($scope, $q, $stateParams, ModalHelper, LoadingHelper, AlertHelper, ActionsHelper, DialogHelper, NavigationHelper, Formatter, LoginService, ProfileService, AthleteService, WorkoutService) {   

    this.athlete = {};
    this.profile = {};
    this.workouts = {};
    this.athlete.sportsSelected = {};
    this.workout = {};

    this.reload = () => {

    	if ($stateParams.idAthlete != null) {
	        var idAthlete = $stateParams.idAthlete;
	        this.getAthlete(idAthlete);

	        if ($stateParams.idWorkout != null) {
	            var idWorkout = $stateParams.idWorkout;
	            this.getWorkout(idWorkout);
	        }
	    } else {
	    	this.getAthlete(1);
	    }

    	$scope.$broadcast('scroll.refreshComplete');
    }

    this.getAthlete = (idAthlete) => {
        var _this = this;
        LoadingHelper.show();
        $q.all({
            athlete : AthleteService.getOne(idAthlete),
            workout : WorkoutService.listByAthlete(idAthlete)
        }).then( d => {
            _this.athlete = d.athlete.data;
            _this.workouts = d.workout.data;

            angular.forEach(_this.workouts, function(value, key) {
				value.scheduleDate = moment(value.scheduleDate).format('DD/MM/YYYY');
            });


            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao recuperar o Atleta!");
        });
    }

    this.viewStepsPopup = (workout) => {
        $scope.currentWorkout = workout;
        ModalHelper.show('templates/workout/view-step-popup.html', $scope);
    }

    this.editWorkout = (workout) => {

        var param = {
            idAthlete: workout.athlete.idAthlete,
            idWorkout: workout.idWorkout
        }

        NavigationHelper.goToStateClear('app.add-workout-steps', param, null);
    }

    this.removeWorkout = (workout) => {

		var _this = this;
		$q.all({
            data : DialogHelper.remove("Deseja remover este Treino?")
        }).then( d => {
            if (d.data) {
		    	LoadingHelper.show();
		        $q.all({
		            workout : WorkoutService.deleteWorkout(workout.idWorkout)
		        }).then( d => {
		        	_this.getAthlete(_this.athlete.idAthlete);
		            LoadingHelper.hide();
		        }, err => {
		            console.error(err);
		            LoadingHelper.hide();
		            AlertHelper.error("Ocorreu um erro ao remover Exercício!");
		        });
            }
        });
    }

    this.reload();
}