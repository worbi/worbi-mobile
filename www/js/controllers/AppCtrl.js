﻿'use strict';

app.controller('AppCtrl', AppCtrl);
AppCtrl.$inject = ['$scope', '$ionicModal', '$ionicPopover', '$timeout', 'LoginService', 'MenuHelper'];

function AppCtrl($scope, $ionicModal, $ionicPopover, $timeout, LoginService, MenuHelper) {    
    /*
    var template = '<ion-popover-view>' +
                    '   <ion-header-bar>' +
                    '       <h1 class="title">My Popover Title</h1>' +
                    '   </ion-header-bar>' +
                    '   <ion-content class="padding">' +
                    '       My Popover Contents' +
                    '   </ion-content>' +
                    '</ion-popover-view>';

    $scope.popover = $ionicPopover.fromTemplate(template, {
        scope: $scope
    });
    $scope.closePopover = function () {
        $scope.popover.hide();
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.popover.remove();
    });*/

    $scope.logout = () => {
        LoginService.logout();
    }

    $scope.getMenu = () => {
        $scope.menuLinks = [];

        LoginService.getLoggedUser((user) => {
            $scope.menuLinks = MenuHelper.getMenu(user.isAdmin);
            $scope.menuName  = (user.name != null) ? user.name : user.email; 
        });
    }

    $scope.getMenu();
    $scope.$on('menu-name', (evt, result) => {
        $scope.menuName = result.data;
    });
    $scope.$on('menu-load', (evt, result) => {
        $scope.menuLinks = result.data;
    });

}