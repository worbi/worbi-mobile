'use strict'

app.factory('CoachService', CoachService);

CoachService.$inject = ['AppConfig', '$http','LoadingHelper'];

function CoachService(AppConfig, $http, LoadingHelper) {

    var urlBase = AppConfig.serviceLayer;
    var coachService = {};


    var services = {
        post : {
            save : urlBase + 'coach/save',
            addAthlete : urlBase + 'coach/{id}/athlete/add'
        },
        get  : {
            search : urlBase + 'coach.user/',
            getOne : urlBase + 'coach/{idCoach}'
        },
        delete : {
            removeAthlete : urlBase + 'coach/{id}/athlete/del'
        }
        
    }

    coachService.save = (coach) => {
        var promisse;
        if (coach != null) {
            promisse = $http.post(services.post.save, coach);
        }
        return promisse;
    }

    coachService.getOne = (idCoach) => {
        var uri = services.get.getOne.replace("{idCoach}",idCoach);
        return $http.get(uri);
    }

    coachService.addAthlete = (idCoach, athlete) => {
        var uri = services.post.addAthlete.replace("{id}",idCoach);
        return $http.post(uri, athlete);
    }

    coachService.removeAthlete = (idCoach, athlete) => {
        var uri = services.delete.removeAthlete.replace("{id}",idCoach);
        return $http.post(uri, athlete);
    }

    coachService.searchByUser = (idUser) => {
        return $http.get(services.get.search + idUser);
    }


    return coachService;
}