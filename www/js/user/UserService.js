'use strict'

app.factory('UserService', UserService);

UserService.$inject = ['AppConfig', '$http'];

function UserService(AppConfig, $http) {

    var urlBase = AppConfig.serviceLayer;
    var userService = {};


    var services = {
        post : {
            save : urlBase + 'user/save',
            changePassword : urlBase + 'user/changepassword',
            resetPassword  : urlBase + 'user/resetpassword'
        },
        get  : {
            list : urlBase + 'user/',
            searchByName : urlBase + 'user-query/'
        },
        delete  : urlBase + 'user/'
    }

    userService.save = (user) => {
        return $http.post(services.post.save, user);
    }

    userService.reset = (email) => {
        return $http.post(services.post.resetPassword, email);
    }

    userService.delete = (user) => {
        return $http.delete(services.delete + user.idUser, {});
    }

    userService.list = () => {
        return $http.get(services.get.list);
    }

    userService.getById = (idUser) => {
        return $http.get(services.get.list + idUser);
    }

    userService.searchByName = (name) => {
        return $http.get(services.get.searchByName + name);
    }

    userService.changePassword = (changePassword) => {
        return $http.post(services.post.changePassword, changePassword);
    }


    return userService;
}