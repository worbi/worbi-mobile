'use strict'

app.factory('ProfileService', ProfileService);

ProfileService.$inject = ['AppConfig', '$http'];

function ProfileService(AppConfig, $http) {

    var urlBase = AppConfig.serviceLayer;
    var profileService = {};


    var services = {
        post : {
            save : urlBase + 'userprofile'
        },
        get  : {
            list    : urlBase + 'profiles/',
            coach   : urlBase + 'coaches/',
            athlete : urlBase + 'athletes/',
            one     : urlBase + 'profile/'
        }
    }

    profileService.list = (query) => {
        return $http.get(services.get.list + query);
    }

    profileService.listCoaches = (query) => {
        return $http.get(services.get.coach + query);
    }

    profileService.listAthletes = (query) => {
        return $http.get(services.get.athlete + query);
    }

    profileService.get = (id) => {
        return $http.get(services.get.one + id);
    }

    profileService.save = (profile) => {
        return $http.post(services.post.save, profile);
    }

    return profileService;
}