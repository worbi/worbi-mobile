'use strict'

app.factory('AthleteService', AthleteService);

AthleteService.$inject = ['AppConfig', '$http'];

function AthleteService(AppConfig, $http) {

    var urlBase = AppConfig.serviceLayer;
    var athleteService = {};


    var services = {
        post : {
            save : urlBase + 'athlete/save', 
            addCoach : urlBase + 'athlete/{id}/coach/add'
        },
        get  : {
            list : urlBase + 'athlete/',
            getOne : urlBase + 'athlete/{id}',
            search : urlBase + 'athlete.user/'
        },
        delete : {
            removeCoach : urlBase + 'athlete/{id}/coach/del'
        }
    }

    athleteService.save = (athlete) => {
        var promisse;
        if (athlete != null) {
            promisse = $http.post(services.post.save, athlete);
        }
        return promisse;
    }

    athleteService.addCoach = (idAthlete, coach) => {
        var addCoachUri = services.post.addCoach.replace("{id}",idAthlete);
        return $http.post(addCoachUri, coach);
    }

    athleteService.removeCoach = (idAthlete, coach) => {
        var removeCoachUri = services.delete.removeCoach.replace("{id}",idAthlete);
        return $http.post(removeCoachUri, coach);
    }

    athleteService.getOne = (idAthlete) => {
        var getOneAthlete = services.get.getOne.replace("{id}",idAthlete);
        return $http.get(getOneAthlete);
    }

    athleteService.list = () => {
        return $http.get(services.get.list);
    }

    athleteService.searchByUser = (idUser) => {
        return $http.get(services.get.search + idUser);
    }


    return athleteService;
}