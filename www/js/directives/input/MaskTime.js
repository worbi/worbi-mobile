app.directive('maskTime', MaskTime)

function MaskTime() {
    return {
        require: "ngModel",
        restrict: "AE",
        scope: {
            ngModel: '=',
        },
        controller: MaskTimeCtrl,
        link: (scope, elem, attrs) => {
            $(elem).mask('000:00', {reverse: true});
        }
   };
}

MaskTimeCtrl.$inject = ['$scope', '$interval'];
function MaskTimeCtrl($scope, $interval) {
}