'use strict';

app.directive('wrbInputTime', WrbInputTime);

function WrbInputTime() {

	return {
		template: '<label class="item item-input item-floating-label currency">'
                   + '<span class="input-label">{{placeholder}}</span>'
                   + '<input type="tel" placeholder="{{placeholder}}" ng-model="ngModel" mask-time>'
                   + '</label>',
		restrict: 'E',
        scope : {
        	placeholder : '@',
        	ngModel     : '=',
          type        : '@'
        },
		controller: WrbInputTimeCtrl,
		link: function (scope, elem, attr) {
		}
    };
}

WrbInputTimeCtrl.$inject = ['$scope'];
function WrbInputTimeCtrl($scope) {

}