'use strict';

app.directive('wrbInputText', WrbInputText);

function WrbInputText() {

	return {
		template: '<label class="item item-input item-floating-label">'
                   + '<span class="input-label">{{placeholder}}</span>'
                   + '<input type="{{type}}" placeholder="{{placeholder}}" ng-model="ngModel">'
                   + '</label>',
		restrict: 'E',
        scope : {
        	placeholder : '@',
        	ngModel     : '=',
          type        : '@'
        },
		controller: WrbInputTextCtrl,
		link: function (scope, elem, attr) {
            
            if (scope.type == null) {
                scope.type = 'text'
            }
		}
    };
}

WrbInputTextCtrl.$inject = ['$scope'];
function WrbInputTextCtrl($scope) {

}