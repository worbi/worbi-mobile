'use strict';

app.directive('wrbInputCheck', WrbInputCheck);

function WrbInputCheck() {

	return {
		template: '<div class="item item-checkbox">'
              +    '<label class="checkbox">'
              +    '<input type="{{type}}" name="{{name}}" value="{{value}}" ng-model="ngModel" >'
              +'</label>'
              +   '{{label}}'
              +'</div>',
		restrict: 'E',
        scope : {
            label   : '@',
            name    : '@',
            value   : '@',
            ngModel : '=',
            type    : '@'
        },
		controller: WrbInputCheckCtrl,
		link: function (scope, elem, attr) {
        }
    };
}

WrbInputCheckCtrl.$inject = ['$scope'];
function WrbInputCheckCtrl($scope) {

}