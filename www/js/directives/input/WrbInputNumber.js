'use strict';

app.directive('wrbInputNumber', WrbInputNumber);

function WrbInputNumber() {

	return {
		template: '<label class="item item-input item-floating-label currency">'
                   + '<span class="input-label">{{placeholder}}</span>'
                   + '<input type="tel" placeholder="{{placeholder}}" ng-model="ngModel" mask-number>'
                   + '</label>',
		restrict: 'E',
        scope : {
        	placeholder : '@',
        	ngModel     : '=',
          type        : '@'
        },
		controller: WrbInputNumberCtrl,
		link: function (scope, elem, attr) {
		}
    };
}

WrbInputNumberCtrl.$inject = ['$scope'];
function WrbInputNumberCtrl($scope) {

}