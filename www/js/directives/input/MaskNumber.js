app.directive('maskNumber', MaskNumber)

function MaskNumber() {
    return {
        require: "ngModel",
        restrict: "AE",
        scope: {
            ngModel: '=',
        },
        controller: MaskNumberCtrl,
        link: (scope, elem, attrs) => {
            $(elem).mask('000.000,00', {reverse: true});
        }
   };
}

MaskNumberCtrl.$inject = ['$scope', '$interval'];
function MaskNumberCtrl($scope, $interval) {
}