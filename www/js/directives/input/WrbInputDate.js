'use strict';

app.directive('wrbInputDate', WrbInputDate);

function WrbInputDate() {

	return {
		template: '<label class="item item-input item-floating-label">'
                   + '<span class="input-label">{{placeholder}}</span>'
                   + '<input type="tel" placeholder="{{placeholder}}" ng-model="ngModel" mask-date }}>'
                   + '</label>',
		restrict: 'E',
        scope : {
        	placeholder : '@',
        	ngModel     : '=',
          type        : '@'
        },
		controller: WrbInputDateCtrl,
		link: function (scope, elem, attr) {

		}
    };
}

WrbInputDateCtrl.$inject = ['$scope'];
function WrbInputDateCtrl($scope) {

}