app.directive('maskDate', MaskDate)

function MaskDate() {
    return {
        require: "ngModel",
        restrict: "AE",
        scope: {
            ngModel: '=',
        },
        controller: MaskDateCtrl,
        link: (scope, elem, attrs) => {
            $(elem).mask('00/00/0000', {reverse: false});
        }
   };
}

MaskDateCtrl.$inject = ['$scope', '$interval'];
function MaskDateCtrl($scope, $interval) {
}