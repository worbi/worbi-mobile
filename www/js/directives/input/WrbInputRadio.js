'use strict';

app.directive('wrbInputRadio', WrbInputRadio);

function WrbInputRadio() {

	return {
		template: '<div class="item item-checkbox">'
              +    '<label class="checkbox">'
              +    '<input type="radio" name="{{name}}" value="{{value}}" ng-model="ngModel" >'
              +'</label>'
              +   '{{label}}'
              +'</div>',
		restrict: 'E',
        scope : {
            label   : '@',
            name    : '@',
            value   : '@',
            ngModel : '='
        },
		controller: WrbInputRadioCtrl,
		link: function (scope, elem, attr) {
        }
    };
}

WrbInputRadioCtrl.$inject = ['$scope'];
function WrbInputRadioCtrl($scope) {

}