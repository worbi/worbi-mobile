app.directive('listAddProfile', ListAddProfile)

function ListAddProfile() {
    return {
        template : '<a ng-click="select(profile.user)" class="item item-icon-left animate-ripple">'
                        +'<i class="icon ion-person"></i> {{profile.user.name}}'
                        +'<p>{{profile.user.email}}</p>'
                        +'<div ng-show="{{showBadge}}">'
                            +'<span class="badge badge-assertive" ng-show="profile.user.isAdmin">Admin</span>'
                            +'<span class="badge badge-positive" ng-show="profile.isAthlete">Atleta</span>'
                            +'<span class="badge badge-balanced" ng-show="profile.isCoach">Treinador</span>'
                        +'</div>'
                    +'</a>',
        restrict : 'E',
        scope: {
            profile : '=',
            showBadge : '@'
        },
        controller: ListAddProfileCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

ListAddProfileCtrl.$inject = ['$scope', '$q', 'LoadingHelper', 'DialogHelper', 'NavigationHelper', 'AlertHelper', 'AthleteService', 'CoachService', 'LoginService', 'ProfileService'];
function ListAddProfileCtrl($scope, $q, LoadingHelper, DialogHelper, NavigationHelper, AlertHelper, AthleteService, CoachService, LoginService, ProfileService) {

    if ($scope.profile.user == null) {
        if ($scope.profile.credential != null) {
            $scope.profile.user = $scope.profile.credential;
        }
    }

    $scope.select = (user) => {

        var confirmMessage = "Deseja Adicionar "+ user.name;
        var success = "";

        if ($scope.profile.isAthlete) { 
            confirmMessage += " como Atleta?";
            success = "Atleta adicionado.";
        } else if ($scope.profile.isCoach) {
            confirmMessage += " como Treinador?";
            success = "Treinador adicionado.";
        }

        LoginService.getLoggedUser((user)=> {
            var loggedId = user.idUser;
        
            $q.all({
                data    : DialogHelper.confirm(confirmMessage),
                profile : ProfileService.get(loggedId)
            }).then( d => {
                
                if (d.data) {

                    var loggedProfile = d.profile.data;
                    if (loggedProfile.isAthlete) {

                        var toAdd = {
                            idCoach : $scope.profile.coach.idCoach
                        }

                        $q.all({
                            addData : AthleteService.addCoach(loggedProfile.athlete.idAthlete, toAdd)
                        }).then( result => {
                            if (result.addData) {
                                NavigationHelper.goToStateClear('app.profile', null, success);
                            }
                        }, err => {
                            console.error(err);
                            var errorMessage = "";
                            LoadingHelper.hide();
                            if (err.status == 400) {
                                errorMessage += "Treinador já adicionado";
                                
                            }
                            AlertHelper.error(errorMessage);
                        });
                    } else if (loggedProfile.isCoach) {

                        var toAdd = {
                            idAthlete : $scope.profile.athlete.idAthlete
                        }
                        
                        $q.all({
                            addData : CoachService.addAthlete(loggedProfile.coach.idCoach, toAdd)
                        }).then( result => {
                            if (result.addData) {
                                NavigationHelper.goToStateClear('app.home', null, success);
                            }
                        }, err => {
                            console.error(err);
                            var errorMessage = "";
                            LoadingHelper.hide();
                            if (err.status == 400) {
                                errorMessage += "Atleta já adicionado";
                                
                            }
                            AlertHelper.error(errorMessage);
                        });
                    }

                }

            });

        });


    }
}