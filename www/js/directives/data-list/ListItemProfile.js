app.directive('listItemProfile', ListItemProfile)

function ListItemProfile() {
    return {
        template : '<a ng-click="goToProfile(profile.user.idUser)" class="item item-icon-left animate-ripple">'
                        +'<i class="icon ion-person"></i> {{profile.user.name}}'
                        +'<p>{{profile.user.email}}</p>'
                        +'<div ng-show="{{showBadge}}">'
                            +'<span class="badge badge-assertive" ng-show="profile.user.isAdmin">Admin</span>'
                            +'<span class="badge badge-positive" ng-show="profile.isAthlete">Atleta</span>'
                            +'<span class="badge badge-balanced" ng-show="profile.isCoach">Treinador</span>'
                        +'</div>'
                    +'</a>',
        restrict : 'E',
        scope: {
            profile  : '=',
            showBadge : '@'
        },
        controller: ListItemProfileCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

ListItemProfileCtrl.$inject = ['$scope', 'NavigationHelper'];
function ListItemProfileCtrl($scope, NavigationHelper) {

    if ($scope.profile.user == null) {
        if ($scope.profile.credential != null) {
            $scope.profile.user = $scope.profile.credential;
        }
    }

    $scope.goToProfile = (idUser) => {
        NavigationHelper.goToState('app.admin-profile', {idUser : idUser});
    }

}