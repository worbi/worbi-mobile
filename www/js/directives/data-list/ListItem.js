app.directive('listItem', ListItem)

function ListItem() {
    return {
        template : '<span class="item in done">'
                        +'<h2><b>{{title}}</b></h2>'
                        +'<p ng-hide="content == \'\'">{{prefix}}{{content}}{{suffix}}</p>'
                        +'<p ng-show="content == \'\'">Não Informado</p>'
                    +'</span>',
        restrict : 'E',
        scope: {
            title   : '@',
            content : '@',
            prefix  : '@',
            suffix  : '@'
        },
        controller: ListItemCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

ListItemCtrl.$inject = ['$scope'];
function ListItemCtrl($scope) {
}