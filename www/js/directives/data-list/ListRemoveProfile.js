app.directive('listRemoveProfile', ListRemoveProfile)

function ListRemoveProfile() {
    return {
        template : '<a ng-click="select(profile.user)" class="item item-icon-left animate-ripple">'
                        +'<i class="icon ion-person"></i> {{profile.user.name}}'
                        +'<p>{{profile.user.email}}</p>'
                        +'<div ng-show="{{showBadge}}">'
                            +'<span class="badge badge-assertive" ng-show="profile.user.isAdmin">Admin</span>'
                            +'<span class="badge badge-positive" ng-show="profile.isAthlete">Atleta</span>'
                            +'<span class="badge badge-balanced" ng-show="profile.isCoach">Treinador</span>'
                        +'</div>'
                    +'</a>',
        restrict : 'E',
        scope: {
            profile : '=',
            showBadge : '@',
            type : "@"
        },
        controller: ListRemoveProfileCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

ListRemoveProfileCtrl.$inject = ['$scope', '$state', '$timeout', '$ionicHistory', '$q', 'LoadingHelper', 'DialogHelper', 'AlertHelper'];
function ListRemoveProfileCtrl($scope, $state, $timeout, $ionicHistory, $q, LoadingHelper, DialogHelper, AlertHelper) {

    if ($scope.profile.user == null) {
        if ($scope.profile.credential != null) {
            $scope.profile.user = $scope.profile.credential;
        }
    }

    $scope.select = (user) => {

        var confirmMessage = "Deseja remover "+ user.name;
        var success = "";

        if ($scope.profile.isAthlete || $scope.type == "athlete") { 
            confirmMessage += " como Atleta?";
            success = "Atleta removido.";
        } else if ($scope.profile.isCoach || $scope.type == "coach") {
            confirmMessage += " como Treinador?";
            success = "Treinador removido.";
        }

        $q.all({
            data : DialogHelper.remove(confirmMessage)
        }).then( d => {
            
            if (d.data) {
                LoadingHelper.show();
                $timeout(function () {
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();
                    $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: false, reload: true });
                    $state.go('app.profile');
                    $scope.$apply();
                    LoadingHelper.hide();
                    AlertHelper.info(success);
                }, 300);
            }

        });
    }
}