app.directive('listOptionsProfile', ListOptionsProfile)

function ListOptionsProfile() {
    return {
        template : '<a ng-click="showOptions(profile.user)" class="item item-icon-left animate-ripple">'
                        +'<i class="icon ion-person"></i> {{profile.user.name}}'
                        +'<p>{{profile.user.email}}</p>'
                        +'<div ng-show="{{showBadge}}">'
                            +'<span class="badge badge-assertive" ng-show="profile.user.isAdmin">Admin</span>'
                            +'<span class="badge badge-positive" ng-show="profile.isAthlete">Atleta</span>'
                            +'<span class="badge badge-balanced" ng-show="profile.isCoach">Treinador</span>'
                        +'</div>'
                    +'</a>',
        restrict : 'E',
        scope: {
            profile   : '=',
            showBadge : '@',
            type      : '@',
            parentId  : '@'
        },
        controller: ListOptionsProfileCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

ListOptionsProfileCtrl.$inject = ['$scope', '$q', 'LoadingHelper', 'DialogHelper', 'AlertHelper', 'NavigationHelper', 'ActionsHelper', 'ModalHelper', 'AthleteService', 'CoachService', 'ProfileService'];
function ListOptionsProfileCtrl($scope, $q, LoadingHelper, DialogHelper, AlertHelper, NavigationHelper, ActionsHelper, ModalHelper, AthleteService, CoachService, ProfileService) {

    if ($scope.profile.user == null) {
        if ($scope.profile.credential != null) {
            $scope.profile.user = $scope.profile.credential;
        }
    }

    $scope.showOptions = (user) => {

        var idUser    = $scope.parentId;
        var isAthlete = ($scope.profile.isAthlete || $scope.type == "athlete");
        var isCoach   = ($scope.profile.isCoach   || $scope.type == "coach");

        var addWorkout = {
            text : '<i class="icon fas fa-plus"></i> Adicionar Treino',
            action: () => {
                NavigationHelper.goToState('app.add-workout', { idAthlete: $scope.profile.idAthlete }, null);
            }
        }
        var viewWorkout = {
            text : '<i class="icon fas fa-eye"></i> Visualizar Treinos',
            action: () => {
                NavigationHelper.goToState('app.edit-workout', { idAthlete: $scope.profile.idAthlete }, null);
            }
        }

        var buttons = [
                { 
                    text: '<i class="icon far fa-envelope"></i> Enviar Mensagem', 
                    action: () => { 

                        $q.all({
                            coach   : CoachService.getOne(idUser),
                            athlete : AthleteService.getOne(idUser)
                        }).then( d => {

                            var fromProfile;
                            if (isAthlete) {
                                fromProfile = d.coach.data;
                            } else {
                                fromProfile = d.athlete.data;
                            }

                            var currentMessage = {
                                to : $scope.profile.user,
                                from : fromProfile.credential
                            }
                            $scope.currentMessage = currentMessage;
                            ModalHelper.show('templates/message/new-message-popup.html', $scope);

                        }, err => {
                            console.error(err);
                            LoadingHelper.hide();
                            AlertHelper.error("Erro ao carregar perfil");
                        });

                    }
                },
                { 
                    text: '<i class="icon fas fa-eye"></i> Visualizar Perfil', 
                    action: () => { 
                        var idUser = $scope.profile.user.idUser;
                        NavigationHelper.goToState('app.profile', { idUser: idUser });
                    }
                },
                { 
                    text: '<span class="assertive">Remover</span>', 
                    action: () => { 

                        var confirmMessage = "Deseja remover "+ user.name;
                        var success = "";

                        if (isAthlete) { 
                            confirmMessage += " como Atleta?";
                            success = "Atleta removido.";
                        } else if (isCoach) {
                            confirmMessage += " como Treinador?";
                            success = "Treinador removido.";
                        }

                        $q.all({
                            data : DialogHelper.remove(confirmMessage)
                        }).then( d => {
                            
                            if (d.data) {

                                if (isCoach) {

                                    var toDelete = {
                                        idCoach : $scope.profile.idCoach
                                    }

                                    $q.all({
                                        deleteData : AthleteService.removeCoach(idUser, toDelete)
                                    }).then( result => {
                                        if (result.deleteData) {
                                            NavigationHelper.goToStateClear('app.profile', null, success);
                                        }
                                    });
                                } else if (isAthlete) {
                                    var toDelete = {
                                        idAthlete : $scope.profile.idAthlete
                                    }

                                    $q.all({
                                        deleteData : CoachService.removeAthlete(idUser, toDelete)
                                    }).then( result => {
                                        if (result.deleteData) {
                                            NavigationHelper.goToStateClear('app.home', null, success);
                                        }
                                    });
                                }
                            }

                        });
                    }
                }
            ];

        if (isAthlete) {
            buttons.unshift(viewWorkout);
            buttons.unshift(addWorkout);
        }

        ActionsHelper.show(buttons);
    }

}