app.directive('workoutList', WorkoutList)

function WorkoutList() {
    return {
        template : '<div class="list card" style="margin-bottom: 0px;" ng-show="workout != false">'
                    +'<div class="item">'
                    +    '<span class="pull-right" ng-show="{{edit}}">'
                    +        '<button class="btn btn-xs" ng-click="editWorkout(workout)">'
                    +            '<span class="fas fa-pencil"></span>'
                    +        '</button>'
                    +        '<button class="btn btn-xs" ng-click="removeWorkout(workout)">'
                    +            '<span class="fas fa-times-circle"></span>'
                    +        '</button>'
                    +    '</span>'
                    +    '<h2>{{workout.description}}</h2>'
                    +    '<p>Data: {{workout.scheduleDate}}</p>'
                    +    '<p>Tempo total: {{workout.totalTime}}</p>'
                    +'</div>'
                    +'<div class="item item-body" style="padding: 0px;" ng-click="viewStepsPopup(workout)">'
                    +    '<a class="item item-avatar item-icon-right in done">'
                    +        '<img src="img/{{workout.sport.name}}.png">'
                    +        '<h2>{{workout.sport.label}}</h2>'
                    +    '</a>'
                    +'</div> '
                    +'<div class="item tabs tabs-secondary tabs-icon-left" ng-show="{{edit}}">' 
                    +    '<a class="tab-item training {{workout.done}}"><i class="icon ion-thumbsup"></i> Realizado</a>'
                    +    '<a ng-show="workout.feedback != null" class="tab-item training true" ng-click="sendFeedbackPopup(workout);"><i class="icon ion-chatbox"></i> Feedback </a>'
                    +    '<a ng-show="workout.feedback == null" class="tab-item training" ng-click="sendFeedbackPopup(workout);"><i class="icon ion-chatbox"></i> Feedback </a>'
                    +'</div>'
                    +'<div class="item tabs tabs-secondary tabs-icon-left" ng-hide="{{edit}}">' 
                    +    '<a class="tab-item training {{workout.done}}" ng-click="flag(workout);"><i class="icon ion-thumbsup"></i> Realizado</a>'
                    +    '<a ng-show="workout.feedback != null" class="tab-item training true" ng-click="sendFeedbackPopup(workout);"><i class="icon ion-chatbox"></i> Feedback </a>'
                    +    '<a ng-show="workout.feedback == null" class="tab-item training" ng-click="sendFeedbackPopup(workout);"><i class="icon ion-chatbox"></i> Feedback </a>'
                    +'</div>'
                +'</div>',
        restrict : 'E',
        scope: {
            workout  : '=',
            edit     : '@'
        },
        controller: WorkoutListCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

WorkoutListCtrl.$inject = ['$scope', '$q', 'Formatter', 'DialogHelper', 'LoadingHelper', 'ModalHelper', 'AlertHelper', 'NavigationHelper', 'WorkoutService'];
function WorkoutListCtrl($scope, $q, Formatter, DialogHelper, LoadingHelper, ModalHelper, AlertHelper, NavigationHelper, WorkoutService) {

    $scope.flag = (workout) => {
        LoadingHelper.show();
        $q.all({
            workout : WorkoutService.flagWorkout(workout.idWorkout)
        }).then( d => {
            $scope.workout = d.workout.data;
            $scope.workout.scheduleDate = moment($scope.workout.scheduleDate).format('DD/MM/YYYY');
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao salvar!");
        });
    }

    $scope.editWorkout = (workout) => {

        var param = {
            idAthlete: workout.athlete.idAthlete,
            idWorkout: workout.idWorkout
        }

        NavigationHelper.goToStateClear('app.add-workout-steps', param, null);
    }

    $scope.viewStepsPopup = (workout) => {
        $scope.currentWorkout = workout;
        ModalHelper.show('templates/workout/view-step-popup.html', $scope);
    }

    $scope.sendFeedbackPopup = (workout) => {
        $scope.currentWorkout = workout;

        if ($scope.edit) {

            if (workout.feedback == null) {
                AlertHelper.info("Nenhum feedback informado para este treino.");
            } else {
                ModalHelper.show('templates/workout/view-feedback-popup.html', $scope);                
            }

        } else {
            ModalHelper.show('templates/workout/add-feedback-popup.html', $scope);
            
        }

    }

    $scope.sendFeedback = (workout) => {

        workout.scheduleDate = Formatter.formatDate(workout.scheduleDate);
        if (workout.feedback == "") {
            workout.feedback = null;
        }

        LoadingHelper.show();
        $q.all({
            workout : WorkoutService.save(workout)
        }).then( d => {
            $scope.workout = d.workout.data;
            $scope.workout.scheduleDate = moment($scope.workout.scheduleDate).format('DD/MM/YYYY');
            LoadingHelper.hide();
        }, err => {
            console.error(err);
            LoadingHelper.hide();
            AlertHelper.error("Ocorreu um erro ao salvar!");
        });
    }    

    $scope.removeWorkout = (workout) => {

        $q.all({
            data : DialogHelper.remove("Deseja remover este Treino?")
        }).then( d => {
            if (d.data) {
                LoadingHelper.show();
                $q.all({
                    workout : WorkoutService.deleteWorkout(workout.idWorkout)
                }).then( d => {
                    $scope.workout = false;
                    LoadingHelper.hide();
                }, err => {
                    console.error(err);
                    LoadingHelper.hide();
                    AlertHelper.error("Ocorreu um erro ao remover Exercício!");
                });
            }
        });
    }

}