app.directive('stepType', StepType)

function StepType() {
    return {
        require: "ngModel",
        template: '{{output}}',
        restrict: "AE",
        scope: {
            ngModel: '=',
        },
        controller: StepTypeCtrl,
        link: (scope, elem, attrs) => {

        }
   };
}

StepTypeCtrl.$inject = ['$scope'];
function StepTypeCtrl($scope) {

    if ($scope.ngModel == "WARMUP") {
        $scope.output = "Aquecimento";
    } else if ($scope.ngModel == "EXERCISE") {
        $scope.output = "Exercício";
    } else if ($scope.ngModel == "RECOVER") {
        $scope.output = "Recuperação";
    } else if ($scope.ngModel == "REST") {
        $scope.output = "Descanso";
    } else if ($scope.ngModel == "COOLDOWN") {
        $scope.output = "Arrefecimento";
    } 

}