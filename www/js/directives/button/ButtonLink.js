'use strict';

app.directive('buttonLink', ButtonLink);

function ButtonLink() {

	return {
		template: '<a href="{{href}}" class="button button-large button-block button-positive {{pluscss}} ink">'
					+ '<button-icon></button-icon>'
					+ '{{label}}'
				    + '</a>',
		restrict: 'E',
        scope : {
        	href    : '@',
        	icon    : '@',
        	label   : '@',
        	iconPosition : '@'
        },
		controller: ButtonLinkCtrl,
		link: function (scope, elem, attr) {
            if (scope.icon == null || scope.iconPosition != "top") {
				elem.find('button-icon').remove();
            }

	        if (scope.iconPosition == "top") {
	        	elem.removeClass("icon-right");
	        	elem.removeClass("icon-left");
	        	elem.removeClass(scope.icon);

            	scope.pluscss = 'button-icontop';
            } else if (scope.iconPosition == "right"){
            	scope.pluscss = 'icon-right ' + scope.icon;            	
            } else if (scope.iconPosition == "left" || scope.icon != null) {
            	scope.pluscss = 'icon-left ' + scope.icon;
            }
		}
    };
}

ButtonLinkCtrl.$inject = ['$scope'];
function ButtonLinkCtrl($scope) {
}

/**
	Button Icon
*/
app.directive('buttonIcon', ButtonIcon);
function ButtonIcon (){
    return{
        restrict:'E',
        require : '^?buttonLink',
        template : '<span class="icon {{icon}}"></span>',
        scope : {
        	icon : '@'
        },
        link:function(scope,elem,attr,outercontrol){
            scope.icon = scope.$parent.icon;
            if (scope.$parent.iconPosition == 'top') {
            	elem.append("<br>");
            } else if (scope.$parent.iconPosition == 'left') {
                elem.append("&nbsp;");
            }
        }
    }
}