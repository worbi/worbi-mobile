app.directive('messageList', MessageList)

function MessageList() {
    return {
        template : '<div class="list card">'
                   +    '<div class="item item-icon-left" ng-repeat="m in messages" ng-click="readMessage(m)">'
                   +         '<p class="pull-right">{{m.sentDate}}</p>'
                   +         '<div ng-show="type==\'to\'">'
                   +             '<i class="icon far fa-envelope fw" ng-hide="m.read"></i>'
                   +             '<i class="icon far fa-envelopepen-o fw" ng-show="m.read" style="padding-left:0px;"></i>'
                   +         '</div>'
                   +         '<div ng-show="type==\'from\'">'
                   +             '<i class="icon ion-person"></i>'
                   +         '</div>'
                   +         '<h2 ng-show="type==\'from\'">{{m.to.name}}</h2>'
                   +         '<h2 ng-show="type==\'to\'">{{m.from.name}}</h2>'
                   +         '<div ng-show="type==\'to\'">'
                   +            '<p ng-hide="m.read"><b ng-bind-html="m.preview"></b></p>'
                   +            '<p ng-show="m.read" ng-bind-html="m.preview"></p>'
                   +         '</div>'
                   +         '<div ng-show="type==\'from\'">'
                   +            '<p ng-bind-html="m.preview"></p>'
                   +         '</div>'
                   +    '</div>'
                   +'</div>',
        restrict : 'E',
        scope: {
            messages : '=',
            type     : '@'
        },
        controller: MessageListCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

MessageListCtrl.$inject = ['$scope', '$q','LoadingHelper', 'AlertHelper', 'ModalHelper', 'Formatter', 'MessageService'];
function MessageListCtrl($scope, $q, LoadingHelper, AlertHelper, ModalHelper, Formatter, MessageService) {

    $scope.readMessage = (message) => {
        $scope.currentMessage = message;
        $scope.replyPanel = true;
        if ($scope.type=="from") {
            $scope.replyPanel = false;
        } else if (!message.read) {

            message.read = true;
            message.sentDate = Formatter.formatDate(message.sentDate);

            LoadingHelper.show();
            $q.all({
                message : MessageService.save(message)
            }).then( d => {
                LoadingHelper.hide();
            }, err => {
                console.error(err);
                LoadingHelper.hide();
                AlertHelper.error("Erro ao salvar mensagem!");
            });
   
        }

        ModalHelper.show('templates/message/read-message-popup.html', $scope);
    }

}