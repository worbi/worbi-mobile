app.directive('messageContainer', MessageContainer)

function MessageContainer() {
    return {
        template : '<ion-refresher on-refresh="reload()"></ion-refresher>'
                   +'<div class="tabs tabs-icon-top static nav nav-tabs" role="tablist">'
                   +    '<a data-target="#tab-message-received" class="tab-item active" data-toggle="tab">'
                   +        '<i class="icon fas fa-arrow-down"></i> Recebidas'
                   +    '</a>'
                   +    '<a data-target="#tab-message-sent" class="tab-item" data-toggle="tab">'
                   +        '<i class="icon fas fa-arrow-up"></i> Enviadas'
                   +    '</a>'
                   +'</div>'
                   +'<div id="messagePanes" class="tab-content">'
                   +    '<div role="tabpanel" class="tab-pane active" id="tab-message-received">'
                   +        '<message-list messages="messagesTo" type="to">'
                   +        '</message-list>'
                   +    '</div>'
                   +    '<div role="tabpanel" class="tab-pane" id="tab-message-sent">'
                   +        '<message-list messages="messagesFrom" type="from">'
                   +        '</message-list>'
                   +    '</div>'
                   +'</div>',
        restrict : 'E',
        scope: {
            user : '=',
        },
        controller: MessageContainerCtrl,
        link: (scope, elem, attrs) => {
        }
   };
}

MessageContainerCtrl.$inject = ['$scope', '$q', 'LoadingHelper', 'AlertHelper', 'MessageService'];
function MessageContainerCtrl($scope, $q, LoadingHelper, AlertHelper, MessageService) {

    $scope.messages = [];
    $scope.loadMessages = () => {
        if ($scope.user != null) {
            $q.all({
                messagesFrom : MessageService.listFrom($scope.user.idUser),
                messagesTo   : MessageService.listTo($scope.user.idUser)
            }).then( d => {
                $scope.messagesFrom = d.messagesFrom.data;
                $scope.messagesTo   = d.messagesTo.data;

                angular.forEach($scope.messagesFrom, function(value, key) {
                    value.sentDate = moment(value.sentDate).format('DD/MM/YYYY');
                    value.preview = value.message;
                    var n = value.message.indexOf("<br/>");
                    if(n > 0) {
                        value.preview = value.message.substring(0, n);
                    }
                });

                angular.forEach($scope.messagesTo, function(value, key) {
                    value.sentDate = moment(value.sentDate).format('DD/MM/YYYY');
                    value.preview = value.message;
                    var n = value.message.indexOf("<br/>");
                    if(n > 0) {
                        value.preview = value.message.substring(0, n);
                    }
                });

                LoadingHelper.hide();
            }, err => {
                console.error(err);
                LoadingHelper.hide();
                AlertHelper.error("Erro ao carregar as mensagens");
            });
        }
    }

    $scope.reload = () => {
        $scope.loadMessages();
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.loadMessages();
}